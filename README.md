# marionettte-based-web-framework

Practice project that I used to learn the ins and outs of web frameworks. Touches on abstraction using TS, event management systems, composition and inheritance as well as abstraction classes and templating.

# Important 

This project does use `pnpm` but you should be fine to use `npm` as well. 


## Installation

1. `pnpm i` for all packages to be installed

## Running the Framework

1. run `pnpm start:db`
2. `pnpm start:parcel`

These should be run in parallel and you should be able to see it in working order.
It currently uses a *User* model to display information to the page. Everything is piped through a single *root* id in *index.html*