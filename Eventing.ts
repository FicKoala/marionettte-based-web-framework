export type Callback = () => void;
export class Eventing {
    events: { [key: string]: Callback[] } = {};
    //say that callback is going to be a function
    //callback registration
    on = (eventName: string, callback: Callback): void => {
        //Fallback is empty array
        //Guarantees is array
        const handlers = this.events[eventName] || [];
        handlers.push(callback);
        this.events[eventName] = handlers;
    }

    trigger = (eventName: string): void =>{
        const handlers = this.events[eventName];
        if (!handlers || handlers.length === 0) {
            return;
        }

        handlers.forEach(callback => {
            callback();
        })
    }


}
