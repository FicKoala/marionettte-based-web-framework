import {Collection} from "./Collection";

export abstract class CollectionView<T,K>{
    constructor(public parent: Element, public collection: Collection<T, K>) {}

    abstract renderItem(model: T, itemParent: Element): void;

    render(): void {
        this.parent.innerHTML = '';
        const templateElement = document.createElement('template');

        //render view directly into element
        for (let model of this.collection.models) {
            const itemParent = document.createElement('div');
            this.renderItem(model, itemParent);
            //Append to collectionViews parent
            templateElement.content.append(itemParent);
        }
        this.parent.append(templateElement.content);
    }
}
