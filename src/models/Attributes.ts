export class Attributes<T> {
    constructor(private data: T) {
    }

    //Adds constraint to limited types of type K
    //Can only be string, number, boolean
    get = <K extends keyof T>(key: K): T[K] => {
        return this.data[key];
    }

    set = (updateData: T): void => {
        Object.assign(this.data, updateData);
    }

    //RETURNING ALL PROPERTIIES
    getAll(): T {
        return this.data;
    }
}
