import {AxiosPromise, AxiosResponse} from "axios";

interface ModelAttributes<T> {
    set(update: T): void;

    getAll(): T;

    get<K extends keyof T>(key: K): T[K];
}

interface ApiSync<T> {
    fetch(id: number): AxiosPromise;

    save(data: T): AxiosPromise;
}

interface Events {
    on(eventName: string, callback: () => void) : void;

    trigger(eventName: string): void;
}

interface HasId {
    uuid?: number;
}

//T specifies type to be passed through
export class Model<T extends HasId> {

    //Reimplement past methods
    on = this.events.on;
    trigger = this.events.trigger;
    get = this.attributes.get;

    constructor(
        private attributes: ModelAttributes<T>,
        private events: Events,
        private apiSync: ApiSync<T>) {
    }

    set(update: T): void {
        this.attributes.set(update);
        this.events.trigger('change');
    }

    fetch(): void {
        const id = this.attributes.get('uuid');

        if (typeof id !== 'number') {
            throw new Error('Could not find uuid');
        }

        this.apiSync.fetch(id).then((response: AxiosResponse): void => {
            //triggers change event
            this.set(response.data);
        }).catch((): void => {
            console.log("Unable to call event. This could be due to myriad things - but check the url first because you're a dickhead and you often copypasta shit wrong")
        })
    }

    save(): void {
        this.apiSync.save(this.attributes.getAll()).then((response: AxiosResponse): void => {
            this.trigger('saved');
        }).catch(() => {
            this.trigger('error');
        });
    }

}
