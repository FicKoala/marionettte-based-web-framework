import axios, {AxiosPromise} from "axios";

interface HasID {
    uuid?: number
}
//Has to pass ID as number
export class ApiSync<T extends HasID>{

    constructor(public rootUrl: string) {}
    fetch(uuid: number): AxiosPromise {
        return axios.get(`${this.rootUrl}${uuid}`)
    }

    save(data: T): AxiosPromise {
        //Creating put request
        //If no id, make post
        const {uuid} = data;
        if (uuid) {
            return axios.put(`${this.rootUrl}/${uuid}`, data);
        }
        //Make POST request if it fails
        else {
            return axios.post(this.rootUrl, data);
        }
    }
}
