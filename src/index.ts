// import {UserForm} from "./views/UserForm";
// import {User} from "./models/User";
// import {UserEdit} from "./views/UserEdit";
//
// const user = User.buildUser({name: 'bob', age: 30});
// const root = document.getElementById('root');
// if (root) {
// const userForm = new UserEdit(root, user);
// userForm.render();
//     console.log(userForm);
// }else {
//     throw new Error('Root element was not found ya donkey)');
// }

//Collection needs T, K then call fetch and populates array followed by event call

import {Collection} from "./models/Collection";
import {User, UserProps} from "./models/User";
import {UserList} from "./views/UserList";

const users = new Collection('http://localhost:3004/movies', (json: UserProps) =>{
    //Pass for specifying model
    return User.buildUser(json)
});

//watch for change event
users.on('change', () => {
    const root = document.getElementById('root');
    if (root) {
        new UserList(root, users).render();
    }
});
users.fetch();
