import {User, UserProps} from "../models/User";
import {View} from "./View";


//User will have set of properties of UserProps via generics
export class UserForm extends View<User, UserProps> {

    eventsMap(): { [key: string]: () => void } {
        return {
            'click:.set-age': this.onSetAgeClick,
            'click:.set-name': this.onSetNameClick,
            'click:.save-model': this.onSaveClick

        }
    }

    template(): string {
        return `
        <div>
        <input placeholder="${this.model.get('name')}" />
        <button>CLICK MAAAAA</button>
        <button class="set-age">Random Age</button>
        <button class="set-name">Set Name</button>
        <button class="save-model">Save</button>
        </div>
               `;
    }

    onSetAgeClick = (): void => {
        this.model.setRandomAge();
    };

    onSetNameClick = (): void => {
        const input = this.parent.querySelector('input')
        if (input) {
            //extract input value from DOM
            const name = input.value;
            this.model.set({name});
        }
    };

    onSaveClick = (): void => {
        this.model.save();
    };
}
