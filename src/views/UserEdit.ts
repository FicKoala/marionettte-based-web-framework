import {View} from "./View";
import {User, UserProps} from "../models/User";
import {UserForm} from "./UserForm";
import {UserShow} from "./UserShow";

export class UserEdit extends View<User, UserProps>{
    //loop through key value properties to find selectors
    //Populates into regions property
    regionsmap(): { [key: string]: string } {
        return {
            userShow: '.user-show',
            userForm: '.user-form'
        };
    }

    //loop through to find divs of types Usershow and Userform
    onRender(): void {
        new UserShow(this.regions.userShow, this.model).render();
        new UserForm(this.regions.userForm, this.model).render();
    }
    //Pass in div user-show as parent element
    template(): string {
        return `
        <div>
        <div class="user-show"></div>
        <div class="user-form"></div>
        </div>
        `;
    }

}
