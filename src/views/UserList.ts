import {CollectionView} from "../models/CollectionView";
import {User, UserProps} from "../models/User";
import {UserShow} from "./UserShow";

export class UserList extends CollectionView<User, UserProps>{
    renderItem(model: User, itemParent: Element): void {
        //Create, build and append to parent element
        new UserShow(itemParent, model).render();
    }
}
