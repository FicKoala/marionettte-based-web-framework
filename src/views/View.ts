import {Model} from "../models/Model";

export abstract class View<T extends Model<K>, K> {
    regions: {[key:string]: Element} = {};

    constructor(public parent: Element, public model: T) {
        this.bindModel();
    }

    abstract template(): string;

    regionsmap(): { [key: string]: string } {
        return {};
    }
    //optional in Show.ts files
    //Provide optional events for each View
    eventsMap(): { [key: string]: () => void }{
        return {};
    };

    bindModel(): void {
        this.model.on('change', () => {
            this.render();
        })
    }

    bindEvents(fragment: DocumentFragment): void {
        const eventsMap = this.eventsMap()

        for (let eventKey in eventsMap) {
            const [eventName, selector] = eventKey.split(':');

            fragment.querySelectorAll(selector).forEach(element => {
                element.addEventListener(eventName, eventsMap[eventKey]);
            })
        }
    }

    //Values form keys will equal elements
    mapRegions(fragment: DocumentFragment): void {
        const regionsMap = this.regionsmap();
        for (let key in regionsMap) {
            const selector = regionsMap[key];
            const element = fragment.querySelector(selector);
            if (element) {
                this.regions[key] = element;
            }
        }
    }
    //view nesting option
     onRender(): void {

    }

    render(): void {
        this.parent.innerHTML = '';
        const templateElement = document.createElement('template');
        templateElement.innerHTML = this.template();

        this.bindEvents(templateElement.content);
        this.mapRegions(templateElement.content);

        //Begin nesting process

        this.onRender()
        this.parent.append(templateElement.content);
    }
}
